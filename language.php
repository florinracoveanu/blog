<?php
session_start();
if(!isset($_SESSION['language'])){
    $_SESSION['language'] = 'ro';
}
if(isset($_POST["submitCheckBox"])){
    $_SESSION['language'] = $_POST['language'];
}
if($_SESSION['language'] == 'ro'){
    $roStatus = 'checked';
    $enStatus = 'unchecked';
    $placeHolder = 'Scrie aici';
    $button = 'Cauta';
    $radio = 'Schimba';
    $categories = 'Categorii';
    $about = 'Despre noi';
    $writeAcomment = 'Lasa un comentariu';
    $name = 'Nume';
    $comment = 'Comentariu';
    $post = 'Posteaza';
}else{
    $roStatus = 'unchecked';
    $enStatus = 'checked';
    $placeHolder = 'Type here';
    $button = 'Search';
    $radio = 'Change';
    $categories = 'Categories';
    $about = 'About us';
    $writeAcomment = 'Write a comment';
    $name = 'Name';
    $comment = 'Comment';
    $post = 'Post';
}?>