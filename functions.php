<?php
function news($newsItem, $key)
{
    ?>
    <div class="panel panel-default" style="min-height: 400px">
        <img src="images/<?php echo $newsItem['image']; ?>" width="100%">
        <div class="panel panel-default" style="background-color:#a6a6a6"><?php echo $newsItem['category'] ?></div>
        <h4 style="color:red"><b><?php echo $newsItem['title']; ?></b></h4>
        <p><a href="newsDescription.php?key=<?php echo $key; ?>" style="color:black"><?php echo $newsItem['short_description']; ?></a></p>
        <p><span style="color:red"><?php echo $newsItem['author']; ?></span>, <?php echo $newsItem['date']; ?> </p>
    </div>
    <?php
}

function newsDescription($newsItem)
{
    ?><hr>
    <p><span style="color:red"><?php echo $newsItem['author']; ?></span>, <?php echo $newsItem['date']; ?> </p>
    <h4 style="color:red"><b><?php echo $newsItem['title']; ?></b></h4>
    <div class="panel text-center">
        <img src="images/<?php echo $newsItem['image']; ?>" width="70%">
    </div>
    <p><?php echo $newsItem['description']; ?></p>
    <?php
}

function sidebar($language)
{
    global $mysqlConnect;
    $result = mysqli_query($mysqlConnect, "SELECT * FROM categories where language = '" . $language . "'");
    $categoriesItems = $result->fetch_all(MYSQLI_ASSOC);
    foreach ($categoriesItems as $categoriesItem) {
        ?>
        <a href="<?php echo $categoriesItem['url']; ?>?category=<?php echo $categoriesItem['title']; ?>" style="color:black"><?php echo $categoriesItem['title']; ?></a>
        <br/><br/>
        <?php
    }
}

function footer($category_id, $language)
{
    global $mysqlConnect;
    $result = mysqli_query($mysqlConnect, "SELECT * FROM footer WHERE category_id = '" . $category_id . "' and language = '" . $language . "'");
    $footerItems = $result->fetch_all(MYSQLI_ASSOC);
    foreach ($footerItems as $footerItem) {
        ?> <a href="<?php echo $footerItem['url']; ?>" style="color:black"><?php echo $footerItem['title']; ?></a>
        <br/><?php

    }
}

function pagination($totalPages, $pageNumber)
{
    ?>
    <div class="row">
        <div class="col-sm-12 text-center">
            <ul class="pagination"><?php
                for ($i = 1; $i <= $totalPages; $i++) {
                    if ($i == $pageNumber) {
                        ?>
                        <li class="active"><a href="index.php?page=<?php echo $i; ?>"><?php echo $i; ?></a>
                        </li><?php
                    } else {
                        ?>
                        <li><a href="index.php?page=<?php echo $i; ?>"><?php echo $i; ?></a></li><?php
                    }
                }
                ?>
            </ul>
        </div>
    </div>
    <?php
}

function menu($language)
{
    global $mysqlConnect;
    $result = mysqli_query($mysqlConnect, "SELECT * FROM menu WHERE language = '" . $language . "'");
    $menuITems = $result->fetch_all(MYSQLI_ASSOC); ?>
    <div class="row sticky">
        <div class="col-sm-12" style="background-color:white">
            <nav class="navbar navbar-inverse" style="background-color: yellow">
                <div class="container-fluid">
                    <ul class="nav navbar-nav">
                        <li><a href="index.php?link=smt" style="color:black"><b>Animal<i>Latest</i>News</b></a></li>
                        <?php
                        foreach ($menuITems as $menuITem) {
                            ?>
                            <li><a href="<?php echo $menuITem['url']; ?>"
                                   style="color:black"><?php echo $menuITem['title']; ?></a></li>
                            <?php
                        } ?>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
    <?php
}

?>
