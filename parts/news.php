<?php
global $mysqlConnect;
$limit = 12;
$newsOnRow = 4;

if (isset($_GET['page'])) {
    $pageNumber = $_GET['page'];
} else {
    $pageNumber = 1;
}
$startElement = ($pageNumber - 1) * $limit;


if(isset($_POST['search'])){
    $_SESSION['search'] = $_POST['search'];
}

//------------------------------------------------------------------------------
if(isset($_GET['category'])){
    $result = mysqli_query($mysqlConnect, "SELECT * FROM news WHERE language = '".$_SESSION['language']."' and  category = '".$_GET['category']."' LIMIT $startElement, $limit");
    $newsItems = $result->fetch_all(MYSQLI_ASSOC);
    $resultTotal = mysqli_query($mysqlConnect, "SELECT * FROM news WHERE language = '".$_SESSION['language']."' and  category = '".$_GET['category']."'");
}else{
    if(isset($_SESSION['search'])){
        $result = mysqli_query($mysqlConnect, "SELECT * FROM news WHERE language = '".$_SESSION['language']."' and (title like '%".$_SESSION['search']."%' or short_description like '%".$_SESSION['search']."%' or description like '%".$_SESSION['search']."%' or category like '%".$_SESSION['search']."%') LIMIT $startElement, $limit");
        $newsItems = $result->fetch_all(MYSQLI_ASSOC);
        $resultTotal = mysqli_query($mysqlConnect, "SELECT * FROM news WHERE language = '".$_SESSION['language']."' and (title like '%".$_SESSION['search']."%' or short_description like '%".$_SESSION['search']."%' or description like '%".$_SESSION['search']."%' or category like '%".$_SESSION['search']."%')");
    }else{
        $result = mysqli_query($mysqlConnect, "SELECT * FROM news WHERE language = '" . $_SESSION['language'] . "' LIMIT $startElement, $limit");
        $newsItems = $result->fetch_all(MYSQLI_ASSOC);
        $resultTotal = mysqli_query($mysqlConnect, "SELECT * FROM news WHERE language = '".$_SESSION['language']."'");
    }
}

$numberOfNews = mysqli_num_rows($resultTotal);
$totalPages = ceil($numberOfNews / $limit);
//------------------------------------------------------------------------------

?>
<div class="col-sm-9" style="background-color:white;">
    <div class="row"><?php
        foreach ($newsItems as $key => $newsItem) {
            if (($key % $newsOnRow == 0) && ($key != 0)) { ?>
    </div>
    <div class="row"><?php
            } ?>
        <div class="col-sm-<?php echo 12 / $newsOnRow; ?> text-center">
            <?php news($newsItem, $newsItem['key_number']); ?>
        </div><?php
        }

        ?>
    </div>
    <?php pagination($totalPages, $pageNumber); ?>

</div>